import * as React from "react"
import Image from "next/image";
import Link from "next/link";
import { Button } from "@/components/ui/button"
import {
  Card,
  CardContent,
  CardDescription,
  CardFooter,
  CardHeader,
  CardTitle,
} from "@/components/ui/card"

type CardWithForm =  {
    image : string,
    title : string
}

export function CardWithForm(props : CardWithForm) {
  return (
    <Card className="w-[300px]">
      <CardContent>
        <form>
          <div className="grid w-full items-center gap-4">
            <Image
              src={props.image}
              width={500}
              height={500}
              alt="Picture of the author"
            />
          </div>
        </form>
      </CardContent>
      <CardHeader>
        <CardTitle className="text-lg"><Link href="">{props.title}</Link></CardTitle>
        <CardDescription></CardDescription>
      </CardHeader>

      {/*<CardFooter className="flex justify-between">*/}

      {/*  <Button>Selengkapnya</Button>*/}
      {/*</CardFooter>*/}
    </Card>
  )
}
