import {NewsResponse} from "@/types/news";

export interface NewsServiceInterface {
  findAll() : Promise<NewsResponse[]>
  findByID() : Promise<any>
}
