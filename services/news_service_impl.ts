import {NewsServiceInterface} from "@/services/news_service";
import {NewsResponse} from "@/types/news";
import News from "@/lib/data";
import prisma from "@/lib/prisma";

class NewsServiceImpl implements NewsServiceInterface{

  async findAll(): Promise<NewsResponse[]> {
    return News
  }

  async findByID(): Promise<any> {
    return prisma.news.findMany()
  }

}
export default NewsServiceImpl



