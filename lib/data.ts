import {NewsResponse} from "@/types/news";

const News : NewsResponse[] = [
  {
    title : "7 Manfaat Kopi Hitam dan Efek Sampingnya untuk Kesehatan",
    image : "https://res.cloudinary.com/dk0z4ums3/image/upload/v1594099343/attached_image/ini-manfaat-konsumsi-kopi-hitam-dan-efek-sampingnya-untuk-kesehatan-0-alodokter.jpg"
  },
  {
    title : "Menanti Janji Hacker Brain Cipher Beri \"Kunci\" PDN Gratis, Sekarang atau Rabu 8,5 Tahun Lagi?",
    image : "https://asset.kompas.com/crops/G_l2_7u4VUnL2K5DB_nZGOfSNHE=/0x0:750x500/740x500/data/photo/2023/07/10/64abf0574bfe3.jpg"
  },
  {
    title : "Timnas U16 Indonesia Vs Vietnam: STY dan Nova Sorot Mental Pemain",
    image : "https://asset.kompas.com/crops/wVfjG5e35XM9g6E0SwXJbLgcoD0=/0x0:4748x3165/750x500/data/photo/2024/07/02/6683bd9db2a19.jpg"
  },
  {
    title : "Kenapa Gula Darah Sewaktu Tinggi? Berikut 9 Penyebabnya…",
    image : "https://asset.kompas.com/crops/XdvIt5MiU3FvWBpp_STQqNfr5lA=/0x0:1000x667/750x500/data/photo/2024/05/14/6643278c1df20.jpg"
  },
  {
    title : "7 Manfaat Kopi Hitam dan Efek Sampingnya untuk Kesehatan",
    image : "https://res.cloudinary.com/dk0z4ums3/image/upload/v1594099343/attached_image/ini-manfaat-konsumsi-kopi-hitam-dan-efek-sampingnya-untuk-kesehatan-0-alodokter.jpg"
  },
  {
    title : "7 Manfaat Kopi Hitam dan Efek Sampingnya untuk Kesehatan",
    image : "https://res.cloudinary.com/dk0z4ums3/image/upload/v1594099343/attached_image/ini-manfaat-konsumsi-kopi-hitam-dan-efek-sampingnya-untuk-kesehatan-0-alodokter.jpg"
  }

]

export default News;
