import {NewsResponse} from "@/types/news";

export default class NewsAPI {
  static getListNews = async () : Promise<{error : any, data : NewsResponse[]}> => {
    const responseAPI = await fetch(`${process.env.NEXT_PUBLIC_API_BASE_URL}/api/news`,  {
      cache: 'no-store',
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
    const responseJson = await responseAPI.json()
    return {
      error : responseJson.response.error,
      data : responseJson.response.data,
    }
  }
}
