"use client"
import Link from "next/link"
import {
  Activity,
  ArrowUpRight,
  CreditCard,
  DollarSign,
} from "lucide-react"
import {
  Avatar,
  AvatarFallback,
  AvatarImage,
} from "@/components/ui/avatar"
import { Badge } from "@/components/ui/badge"
import { Button } from "@/components/ui/button"
import {
  Card,
  CardContent,
  CardDescription,
  CardHeader,
  CardTitle,
} from "@/components/ui/card"
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableHeader,
  TableRow,
} from "@/components/ui/table"
import { CardWithForm } from "@/components/content-card"
import News from "@/lib/data";
import {NewsResponse} from "@/types/news";
import {useEffect, useState} from "react";
import NewsAPI from "@/lib/news_api";

export default function Dashboard() {
  const [listNews, setListNews] = useState<NewsResponse[]>([])
  const fetchData = async () => {
    const response = await NewsAPI.getListNews()
    // if (response.header.error) {
    //   return
    // }
    setListNews(response.data)
  }

  useEffect(() => {
    fetchData()
  }, [])

  return (
    <div className="flex min-h-screen w-full flex-col">
      <main className="flex flex-1 flex-col gap-4 p-4 md:gap-8 md:p-8">
        <p className="text-lg font-bold">Rekomendasi untuk anda</p>
        <div className="grid grid-cols-10 gap-4">
          <div className="col-span-7 grid grid-cols-3 gap-1 gap-y-5">
            {listNews?.map((news, idx) => (
              <CardWithForm image={news.image} title={news.title} />
            ))}
          </div>
          <div className="col-span-3 border h-[30rem]">
            <CardHeader>
              <CardTitle>Recent Sales</CardTitle>
            </CardHeader>
            <CardContent className="grid gap-9">
              <div className="flex items-center gap-4">
                <Avatar className="hidden h-9 w-9 sm:flex">
                  <AvatarImage src="/avatars/01.png" alt="Avatar" />
                  <AvatarFallback>OM</AvatarFallback>
                </Avatar>
                <div className="grid gap-1">
                  <p className="text-sm font-medium leading-none">
                    Olivia Martin
                  </p>
                  <p className="text-sm text-muted-foreground">
                    olivia.martin@email.com
                  </p>
                </div>
              </div>
              <div className="flex items-center gap-4">
                <Avatar className="hidden h-9 w-9 sm:flex">
                  <AvatarImage src="/avatars/02.png" alt="Avatar" />
                  <AvatarFallback>JL</AvatarFallback>
                </Avatar>
                <div className="grid gap-1">
                  <p className="text-sm font-medium leading-none">
                    Jackson Lee
                  </p>
                  <p className="text-sm text-muted-foreground">
                    jackson.lee@email.com
                  </p>
                </div>
                <div className="ml-auto font-medium">+$39.00</div>
              </div>
            </CardContent>
          </div>
        </div>
      </main>
    </div>
  )
}
