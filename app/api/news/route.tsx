import {NextRequest, NextResponse} from "next/server";
import NewsServiceImpl from "@/services/news_service_impl";
import {ResponseAPI} from "@/types/response_api";
const newsService = new NewsServiceImpl()
export async function GET(request : NextRequest){
  const response : ResponseAPI = {error : null, data : ''}
  response.data = await newsService.findAll()
  return NextResponse.json({response}, {status : 200})
}

export async function POST(request : NextRequest){
  const response : ResponseAPI = {error : null, data : ''}
  response.data = await newsService.findByID()
  return NextResponse.json({response}, {status : 200})
}
