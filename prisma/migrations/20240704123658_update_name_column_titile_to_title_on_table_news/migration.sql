/*
  Warnings:

  - You are about to drop the column `titile` on the `news` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "news" DROP COLUMN "titile",
ADD COLUMN     "title" VARCHAR;
